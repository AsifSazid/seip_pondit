<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UnitsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Size::create([
            'title' => 'Small'
        ]);

        \App\Models\Size::create([
            'title' => 'Medium'
        ]);
        
        \App\Models\Size::create([
            'title' => 'Large'
        ]);
    }
}
