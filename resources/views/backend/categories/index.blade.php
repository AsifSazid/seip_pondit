<x-backend.layouts.master>
    <x-slot name="pageTitle">
        Categories
    </x-slot>

    <x-slot name='breadCrumb'>
        <x-backend.layouts.elements.breadcrumb>
            <x-slot name="pageHeader"> Categories </x-slot>

            <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
            <li class="breadcrumb-item active">Categories</li>

        </x-backend.layouts.elements.breadcrumb>
    </x-slot>

    <div class="card mb-4">
        <div class="card-header">
            <div class="row">
                <div class="col-9">
                    <i class="fas fa-table me-1"></i>
                    Categories 

                    @can('create-category')
                    <a class="btn btn-sm btn-info" href="{{ route('categories.create') }}">Add New</a>
                    @endcan

                </div>
                <div class="col-3">
                    <form method= "GET" action="{{ route('categories.index')}}">
                        <x-backend.form.input style="width: 200px; height: 20px" name="search" /> 
                    </form>
                </div>
            </div>
        </div>
        <div class="card-body">

            @if (session('message'))
            <div class="alert alert-success">
                <span class="close" data-dismiss="alert">&times;</span>
                <strong>{{ session('message') }}.</strong>
            </div>
            @endif

            <table class="table">
                <thead>
                    <tr>
                        <th class="col-1">Sl#</th>
                        <th class="col-2">Category Name</th>
                        <th class="col-7">Product-List</th>
                        <th class="col-2">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @php $sl=0 @endphp
                    @foreach ($categories as $category)
                    <tr>
                        <td>{{ ++$sl }}</td>
                        <td>{{ $category->title }}</td>
                        <td><a class="btn btn-info btn-sm" href="{{ route('categories.show', ['category' => $category->id]) }}" >Show Products</a></td>
                        <td>

                            <a class="btn btn-warning btn-sm" href="{{ route('categories.edit', ['category' => $category->id]) }}" >Edit</a>

                            <form style="display:inline" action="{{ route('categories.destroy', ['category' => $category->id]) }}" method="post">
                                @csrf
                                @method('delete')
                                
                                <button onclick="return confirm('Are you sure want to delete ?')" class="btn btn-sm btn-danger" type="submit">Delete</button>
                            </form>

                            <!-- <a href="{{ route('categories.destroy', ['category' => $category->id]) }}" >Delete</a> -->


                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $categories-> links() }}
        </div>
    </div>

</x-backend.layouts.master>