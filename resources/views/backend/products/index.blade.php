<x-backend.layouts.master>
    <x-slot name="pageTitle">
        Products
    </x-slot>

    <x-slot name='breadCrumb'>
        <x-backend.layouts.elements.breadcrumb>
            <x-slot name="pageHeader"> Products </x-slot>

            <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
            <li class="breadcrumb-item active">Products</li>

        </x-backend.layouts.elements.breadcrumb>
    </x-slot>

    <div class="card mb-4">
        <div class="card-header">
            <div class="row">
                <div class="col-9">
                    <i class="fas fa-table me-1"></i>
                    Products
                    <a class="btn btn-sm btn-danger" href="{{ route('products.trashed') }}">Trashed List</a>
                    @can('create-product')
                    <a class="btn btn-sm btn-info" href="{{ route('products.create') }}">Add New</a>
                    @endcan
                </div>
                <div class="col-3">
                    <form method= "GET" action="{{ route('categories.index')}}">
                        <x-backend.form.input style="width: 200px; height: 20px" name="search" /> 
                    </form>
                </div>
            </div>
        </div>
        <div class="card-body">

            @if (session('message'))
            <div class="alert alert-success">
                <span class="close" data-dismiss="alert">&times;</span>
                <strong>{{ session('message') }}.</strong>
            </div>
            @endif

            <table class="table">
                <thead>
                    <tr>
                        <th class="col-1">Sl#</th>
                        <th class="col-2">Product Name</th>
                        <th class="col-2">Category</th>
                        <th class="col-7">Description</th>
                        <th class="col-2">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @php $sl=0 @endphp
                    @foreach ($products as $product)
                    <tr>
                        <td>{{ ++$sl }}</td>
                        <td>{{ $product->title }}</td>
                        <td>{{ $product->category->title }}</td>
                        <td>{{ $product->description }}</td>
                        <td>
                            <a class="btn btn-info btn-sm" href="{{ route('products.show', ['product' => $product->id]) }}" >Show</a>

                            <a class="btn btn-warning btn-sm" href="{{ route('products.edit', ['product' => $product->id]) }}" >Edit</a>

                            <form style="display:inline" action="{{ route('products.destroy', ['product' => $product->id]) }}" method="post">
                                @csrf
                                @method('delete')
                                
                                <button onclick="return confirm('Are you sure want to delete ?')" class="btn btn-sm btn-danger" type="submit">Delete</button>
                            </form>

                            
                        </td>
                    </tr>
                    @endforeach

                </tbody>
            </table>
            {{ $products-> links() }}
        </div>
    </div>

</x-backend.layouts.master>