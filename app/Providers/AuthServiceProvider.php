<?php

namespace App\Providers;

use App\Models\User;
use App\Policies\CategoryPolicy;
use App\Policies\ProductPolicy;
use App\Policies\RolePolicy;
use App\Policies\SizePolicy;
use App\Policies\UnitPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        
        Gate::define('create-category', [CategoryPolicy::class, 'create']);
        Gate::define('create-product', [ProductPolicy::class, 'create']);
        Gate::define('create-role', [RolePolicy::class, 'create']);
        Gate::define('create-size', [SizePolicy::class, 'create']);
        Gate::define('create-unit', [UnitPolicy::class, 'create']);

        Gate::define('user-management', function (User $user) {
           
            if ($user->role_id == 1) {
                return true;
            }

            return false;
        });

    }

}

