<?php

namespace App\Http\Controllers;

use App\Models\Size;
use Doctrine\DBAL\Query\QueryException;
use Illuminate\Http\Request;


class SizeController extends Controller
{  
    public function index()
    {
        $sizes = Size::latest()->get();
        return view('backend.sizes.index', [
            'sizes' => $sizes 
        ]);
    }

    public function create()
    {
        $this->authorize('create-size');
        return view('backend.sizes.create');
    }

    public function store(Request $request)
    {
        try{
            request()->validate([
                'title' => 'required|min:3|max:10',
            ]);
    
            Size::create([
                'title' => request()->title,
            ]);
            // $request->session()->flash('message', 'Task was successful!');
            return redirect()->route('sizes.index')->withMessage('Task was successful!');
    

        }catch (QueryException $e) {
            return redirect()-> back()-> withInput()-> withErrors($e-> getMessage());
            dd($e-> getMessage());
        }
        
    }

    public function show(Size $size)
    {
        return view('backend.sizes.show', [
            'size' => $size
        ]);
    }
    public function edit(Size $size)
    {
        return view('backend.sizes.edit', [
            'size' => $size
        ]);
    }

    public function update(Request $request, Size $size)
    {
        try {
            $request->validate([
                'title' => 'required|min:3|max:50|unique:categories,title,'.$size->id,
            ]);

            $size->update([
                'title' => $request->title,
            ]);

            // $request->session()->flash('message', 'Task was successful!');
            return redirect()->route('sizes.index')->withMessage('Successfully Updated!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function destroy(Size $size)
    {
        try {
            $size->delete();
            return redirect()->route('sizes.index')->withMessage('Successfully Deleted!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
            
        }
    }

}
