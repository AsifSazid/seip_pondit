<?php

namespace App\Http\Controllers;

use App\Models\Unit;
use Doctrine\DBAL\Query\QueryException;
use Illuminate\Http\Request;


class UnitController extends Controller
{  
    public function index()
    {
        $units = Unit::latest()->get();
        return view('backend.units.index', [
            'units' => $units 
        ]);
    }

    public function create()
    {
        $this->authorize('create-unit');
        return view('backend.units.create');
    }

    public function store(Request $request)
    {
        // dd(request()->description);
        try{
            request()->validate([
                'title' => 'required|min:3|max:10',
            ]);
    
            Unit::create([
                'title' => request()->title,
            ]);
            // $request->session()->flash('message', 'Task was successful!');
            return redirect()->route('units.index')->withMessage('Task was successful!');
    

        }catch (QueryException $e) {
            return redirect()-> back()-> withInput()-> withErrors($e-> getMessage());
            dd($e-> getMessage());
        }
        
    }

    public function show(Unit $unit)
    {
        return view('backend.units.show', [
            'unit' => $unit
        ]);
    }
    public function edit(Unit $unit)
    {
        return view('backend.units.edit', [
            'unit' => $unit
        ]);
    }

    public function update(Request $request, Unit $unit)
    {
        try {
            $request->validate([
                'title' => 'required|min:3|max:50|unique:categories,title,'.$unit->id,
            ]);

            $unit->update([
                'title' => $request->title,
            ]);

            // $request->session()->flash('message', 'Task was successful!');
            return redirect()->route('units.index')->withMessage('Successfully Updated!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
            // dd($e->getMessage());
        }
    }

    public function destroy(Unit $unit)
    {
        try {
            $unit->delete();
            return redirect()->route('units.index')->withMessage('Successfully Deleted!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
            // dd($e->getMessage());
        }
    }

}
