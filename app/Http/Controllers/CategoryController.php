<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Doctrine\DBAL\Query\QueryException;
use Illuminate\Http\Request;

class CategoryController extends Controller
{  
    public function index()
    {
        $categoriesCollection = Category::latest();
        if(request('search')){
            $categories = Category::latest()
                                ->where('description', 'like', '%'.request('search').'%')
                                ->where('title', 'like', '%'.request('search').'%');
        }
        $categories = $categoriesCollection->paginate(10);

        return view('backend.categories.index', [
            'categories' => $categories 
        ]);
    }

    public function create()
    {
        $this->authorize('create-category');

        return view('backend.categories.create');
    }

    public function store(Request $request)
    {
        // dd(request()->description);
        try{
            request()->validate([
                'title' => 'required|min:3|max:50'
            ]);
    
            Category::create([
                'title' => request()->title
            ]);
            // $request->session()->flash('message', 'Task was successful!');
            return redirect()->route('categories.index')->withMessage('Task was successful!');
    

        }catch (QueryException $e) {
            return redirect()-> back()-> withInput()-> withErrors($e-> getMessage());
            dd($e-> getMessage());
        }
        
    }

    // public function show($id)
    // {
    //     $category = Category::findOrfail($id);
    //     dd ($category);
    // }
    public function show(Category $category)
    {

        return view('backend.categories.show', compact('category'));
    }
    public function edit(Category $category)
    {
        return view('backend.categories.edit', [
            'category' => $category
        ]);
    }

    public function update(Request $request, Category $category)
    {
        try {
            $request->validate([
                'title' => 'required|min:3|max:50|unique:categories,title,'.$category->id,
                'description' => ['required', 'min:10'],
            ]);

            $category->update([
                'title' => $request->title,
                'description' => $request->description,
            ]);

            // $request->session()->flash('message', 'Task was successful!');
            return redirect()->route('categories.index')->withMessage('Successfully Updated!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
            // dd($e->getMessage());
        }
    }

    public function destroy(Category $category)
    {
        try {
            $category->delete();
            return redirect()->route('categories.index')->withMessage('Successfully Deleted!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
            // dd($e->getMessage());
        }
    }



}
