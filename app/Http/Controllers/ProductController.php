<?php

namespace App\Http\Controllers;

use App\Exports\CategoryExport;
use App\Http\Requests\ProductRequest;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Image;

class ProductController extends Controller
{
    public function index()
    {
        $productsCollection = Product::latest();
        if(request('search')){
            $products = Product::latest()
                                ->where('description', 'like', '%'.request('search').'%')
                                ->where('title', 'like', '%'.request('search').'%');
        }
        $products = $productsCollection->paginate(2);
        
        return view('backend.products.index', [
            'products' => $products 
        ]);
    }

    public function create()
    {
        $this->authorize('create-product');
        $categories = Category::select('id', 'title')->get();
        return view('backend.products.create', compact('categories'));
    }

    public function store(ProductRequest $request)
    {
        // dd($request->all());
        try {
           
            Product::create([
                'title' => $request->title,
                'category_id' => $request->category,
                'description' => $request->description,
                'image' => $this->uploadImage(request()->file('image'))
            ]);

            return redirect()->route('products.index')->withMessage('Successfully Created!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
          
        }
    }

    public function show(Product $product)
    {
        return view('backend.products.show', [
            'product' => $product
        ]);
    }

    public function edit(Product $product)
    {
        return view('backend.products.edit', [
            'product' => $product
        ]);
    }

    public function update(ProductRequest $request, Product $product)
    {
        try {
            $product->update([
                'title' => $request->title,
                'description' => $request->description,
                'image'=>$this->uploadImage(request()->file('image'))
            ]);

            return redirect()->route('products.index')->withMessage('Successfully Updated!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());

        }
    }

    public function destroy(Product $product)
    {
        try {
            $product->delete();
            return redirect()->route('products.index')->withMessage('Successfully Deleted!');
        } catch (QueryException $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    // Softdelete
    public function trash()
    {
        $products = Product::onlyTrashed()->get();
        return view('backend.products.trashed', [
            'products' => $products
        ]);        
    }

    public function restore($id)
    {
        $product = Product::onlyTrashed()->findOrFail($id);
        $product->restore();
        return redirect()->route('products.trashed')->withMessage('Successfully Restored!');
    }

    public function delete($id)
    {
        $product = Product::onlyTrashed()->findOrFail($id);
        $product->forceDelete();
        return redirect()->route('products.trashed')->withMessage('Successfully Deleted Permanently!');
    }

    public function uploadImage($file)
    {
        $fileName = time().'.'.$file->getClientOriginalExtension();

        Image::make($file)
                        ->resize(200, 200)
                        ->save(storage_path().'/app/public/images/'.$fileName);

        return $fileName;
    }
}
