<?php

namespace App\Http\Controllers;

use App\Models\Role;
use Doctrine\DBAL\Query\QueryException;
use Illuminate\Http\Request;

class RoleController extends Controller
{  
    public function index()
    {
        $rolesCollection = Role::latest();
        if(request('search')){
            $roles = Role::latest()
                                ->where('description', 'like', '%'.request('search').'%')
                                ->where('title', 'like', '%'.request('search').'%');
        }
        $roles = $rolesCollection->paginate(2);

        return view('backend.roles.index', [
            'roles' => $roles 
        ]);
    }

    public function create()
    {
        $this->authorize('create-Role');

        return view('backend.roles.create');
    }

    public function store(Request $request)
    {
        // dd(request()->description);
        try{
            request()->validate([
                'title' => 'required|min:3|max:10',
                'description' => 'required|min:20|max:250'
            ]);
    
            Role::create([
                'title' => request()->title,
                'description' => $request->description,
            ]);
            // $request->session()->flash('message', 'Task was successful!');
            return redirect()->route('roles.index')->withMessage('Task was successful!');
    

        }catch (QueryException $e) {
            return redirect()-> back()-> withInput()-> withErrors($e-> getMessage());
            dd($e-> getMessage());
        }
        
    }

    // public function show($id)
    // {
    //     $Role = Role::findOrfail($id);
    //     dd ($Role);
    // }
    public function show(Role $Role)
    {
        return view('backend.roles.show', [
            'Role' => $Role
        ]);
        dd ($Role);
    }
    public function edit(Role $Role)
    {
        return view('backend.roles.edit', [
            'Role' => $Role
        ]);
    }

    public function update(Request $request, Role $Role)
    {
        try {
            $request->validate([
                'title' => 'required|min:3|max:50|unique:roles,title,'.$Role->id,
                'description' => ['required', 'min:10'],
            ]);

            $Role->update([
                'title' => $request->title,
                'description' => $request->description,
            ]);

            // $request->session()->flash('message', 'Task was successful!');
            return redirect()->route('roles.index')->withMessage('Successfully Updated!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
            // dd($e->getMessage());
        }
    }

    public function destroy(Role $Role)
    {
        try {
            $Role->delete();
            return redirect()->route('roles.index')->withMessage('Successfully Deleted!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
            // dd($e->getMessage());
        }
    }



}
