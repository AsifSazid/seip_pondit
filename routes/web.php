<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\SizeController;
use App\Http\Controllers\UnitController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\WelcomeController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('/dashboard', function () {
//     return view('dashboard');
// })->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';

Route::middleware('auth')->group(function (){

    Route::get('/home', function(){
        return view('backend.home');
    });
    
    
    Route::get('/trashed-products', [ProductController::class, 'trash'])->name('products.trashed');
    
    Route::get('/trashed-products/{product}/restore', [ProductController::class, 'restore'])->name('products.restore');
    
    Route::delete('/trashed-products/{product}/delete', [ProductController::class, 'delete'])->name('products.delete');
    
    Route::resource('products', ProductController::class);
   
   
   
   
    Route::resource('roles', RoleController::class);
    // Route::get('/roles', [RoleController::class, 'index'])->name('roles.index');
    
    
    
    
    Route::get('/categories', [CategoryController::class, 'index'])->name('categories.index');
    
    Route::get('/categories/create', [CategoryController::class, 'create'])->name('categories.create');
    
    Route::post('/categories', [CategoryController::class, 'store'])->name('categories.store');
    
    Route::get('/categories/{category}', [CategoryController::class, 'show'])->name('categories.show');
    
    // GET	/photos/{photo}/edit	edit	photos.edit
    Route::get('/categories/{category}/edit', [CategoryController::class, 'edit'])->name('categories.edit');
    
    // PUT/PATCH	/photos/{photo} update	photos.update
    Route::patch('/categories/{category}', [CategoryController::class, 'update'])->name('categories.update');
    
    // DELETE	/photos/{photo}	destroy	photos.destroy
    Route::delete('/categories/{category}', [CategoryController::class, 'destroy'])->name('categories.destroy');
    
    
    
    
    
    
    Route::get('/sizes', [SizeController::class, 'index'])->name('sizes.index');
    
    Route::get('/sizes/create', [SizeController::class, 'create'])->name('sizes.create');
    
    Route::post('/sizes', [SizeController::class, 'store'])->name('sizes.store');
    
    Route::get('/sizes/{size}', [SizeController::class, 'show'])->name('sizes.show');
    
    // GET	/photos/{photo}/edit	edit	photos.edit
    Route::get('/sizes/{size}/edit', [SizeController::class, 'edit'])->name('sizes.edit');
    
    // PUT/PATCH	/photos/{photo} update	photos.update
    Route::patch('/sizes/{size}', [SizeController::class, 'update'])->name('sizes.update');
    
    // DELETE	/photos/{photo}	destroy	photos.destroy
    Route::delete('/sizes/{size}', [SizeController::class, 'destroy'])->name('sizes.destroy');
    
    
    
    
    
    Route::get('/units', [UnitController::class, 'index'])->name('units.index');
    
    Route::get('/units/create', [UnitController::class, 'create'])->name('units.create');
    
    Route::post('/units', [UnitController::class, 'store'])->name('units.store');
    
    Route::get('/units/{unit}', [UnitController::class, 'show'])->name('units.show');
    
    // GET	/photos/{photo}/edit	edit	photos.edit
    Route::get('/units/{unit}/edit', [UnitController::class, 'edit'])->name('units.edit');
    
    // PUT/PATCH	/photos/{photo} update	photos.update
    Route::patch('/units/{unit}', [UnitController::class, 'update'])->name('units.update');
    
    // DELETE	/photos/{photo}	destroy	photos.destroy
    Route::delete('/units/{unit}', [UnitController::class, 'destroy'])->name('units.destroy');
    

});